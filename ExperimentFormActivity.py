from PyQt5 import QtGui
from PyQt5 import QtWidgets


class ExperimentForm(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__()
        sshFile = "experiment_form.less"

        with open(sshFile, "r") as fh:
            self.setStyleSheet(fh.read())

        header = self.add_radio_buttons()
        title = QtWidgets.QLabel("შეიყვანეთ მონაცემები ექსპერიმენტის დასაწყებად:")

        self.stack = QtWidgets.QStackedWidget(self)
        self.stack.setStyleSheet("background-color: #dadada")
        self.stack.addWidget(self.add_water_layer())
        self.stack.addWidget(self.add_blood_layout())

        footer = self.add_footer()

        wind_layout = QtWidgets.QVBoxLayout()
        wind_layout.setContentsMargins(30, 30, 30, 30)

        wind_layout.addLayout(header, 2)
        wind_layout.addWidget(title, 1)
        wind_layout.addWidget(self.stack, 5)
        wind_layout.addLayout(footer)

        self.WaterRadio.click()

        self.setLayout(wind_layout)
        self.setWindowTitle("Calorimeter v6.9")
        self.setGeometry(300, 300, 640, 640)

    def add_radio_buttons(self):
        self.WaterRadio = QtWidgets.QRadioButton("წყალი")
        self.BloodRadio = QtWidgets.QRadioButton("პლაზმა")

        self.WaterRadio.setStyleSheet("color: #5a9eb2; font-size: 30px")
        self.BloodRadio.setStyleSheet("color: #be203f; font-size: 30px")

        hor = QtWidgets.QHBoxLayout()
        hor.addWidget(self.WaterRadio)
        hor.addWidget(self.BloodRadio)

        # self.WaterRadio.click()
        self.WaterRadio.clicked.connect(lambda: self.display_layout(0))
        self.BloodRadio.clicked.connect(lambda: self.display_layout(1))

        return hor

    def add_footer(self):
        h_box = QtWidgets.QHBoxLayout()
        self.back_button = QtWidgets.QPushButton("უკან")
        self.next_button = QtWidgets.QPushButton("დაწყება")

        self.back_button.setObjectName("backButton")
        self.next_button.setObjectName("nextButton")
        h_box.addWidget(self.back_button)
        h_box.addStretch()
        h_box.addWidget(self.next_button)
        self.next_button.clicked.connect(self.on_next_pressed)
        return h_box

    def add_water_layer(self):
        w = QtWidgets.QWidget()
        grid = QtWidgets.QGridLayout()

        self.water_name_filed = QtWidgets.QLineEdit("შეიყვანეთ სახელი")
        self.water_exp_num_field = QtWidgets.QLineEdit("შეიყვანეთ ნომერი")
        self.water_exp_num_field.setValidator(QtGui.QDoubleValidator())
        self.water_start_temp_field = QtWidgets.QDoubleSpinBox()
        self.water_end_temp_field = QtWidgets.QDoubleSpinBox()
        self.water_details_filed = QtWidgets.QTextEdit()

        widgets = [QtWidgets.QLabel("ექსპერიმენტის სახელი"), QtWidgets.QLabel("ნიმუშის ნომერი"),
                   self.water_name_filed, self.water_exp_num_field,
                   QtWidgets.QLabel("საწყისი ტემპერატურა"), QtWidgets.QLabel("საბოლოო ტემპერატურა"),
                   self.water_start_temp_field, self.water_end_temp_field,
                   QtWidgets.QLabel("დამატებითი ინფორმაცია"), '',
                   self.water_details_filed]

        self.fill_grid(grid, widgets, 2, 6)
        w.setLayout(grid)
        return w

    def add_blood_layout(self):
        w = QtWidgets.QWidget()
        grid = QtWidgets.QGridLayout()

        self.blood_exp_model = QtWidgets.QComboBox(self)
        self.blood_exp_model.setStyleSheet("background-color: white; color: black")
        self.blood_exp_model.addItem("ექსპერიმენტი 10")
        self.blood_exp_model.addItem("ექსპერიმენტი 9")
        self.blood_exp_model.addItem("ექსპერიმენტი 8")
        self.blood_exp_model.addItem("ექსპერიმენტი 7")
        self.blood_exp_model.addItem("ექსპერიმენტი 6")
        self.blood_exp_model.addItem("ექსპერიმენტი 5")
        self.blood_exp_model.addItem("ექსპერიმენტი 4")
        self.blood_exp_model.addItem("ექსპერიმენტი 3")
        self.blood_exp_model.addItem("ექსპერიმენტი 2")
        self.blood_exp_model.addItem("ექსპერიმენტი 1")
        self.blood_exp_num_field = QtWidgets.QLineEdit("შეიყვანეთ ნომერი")
        self.blood_exp_num_field.setValidator(QtGui.QDoubleValidator())
        self.blood_details_field = QtWidgets.QTextEdit()

        widgets = [QtWidgets.QLabel("აირჩიეთ ექსპერიმენტი"),
                   self.blood_exp_model,
                   QtWidgets.QLabel("ნიმუშის ნომერი"),
                   self.blood_exp_num_field,
                   QtWidgets.QLabel("დამატებითი ინფორმაცია"),
                   self.blood_details_field]

        self.fill_grid(grid, widgets, 1, 6)
        w.setLayout(grid)
        return w

    def fill_grid(self, grid, widgets, width, height):
        positions = [(i, j) for i in range(height) for j in range(width)]

        for position, widget in zip(positions, widgets):
            if widget == '':
                continue
            elif isinstance(widget, QtWidgets.QLineEdit):
                widget.setPlaceholderText(widget.text())
                widget.setText("")
                widget.setMaximumWidth(300)
                widget.setStyleSheet("background-color: white")
            elif isinstance(widget, QtWidgets.QDoubleSpinBox):
                widget.setStyleSheet("background-color: white")
                widget.setRange(0, 150)
                widget.setMaximumWidth(300)
            elif isinstance(widget, QtWidgets.QTextEdit):
                widget.setStyleSheet("background-color: white")
                grid.addWidget(widget, *position, 5, 2)
                continue
            grid.addWidget(widget, *position)

    def display_layout(self, i):
        self.stack.setCurrentIndex(i)

    def on_back_pressed(self):
        print("BACK")

    def on_next_pressed(self):
        print("----------------------------")
        print("Name: " + self.water_name_filed.text())
        print("Number: " + self.water_exp_num_field.text())
        print("Start Temp: " + str(self.water_start_temp_field.value()))
        print("End Temp: " + str(self.water_end_temp_field.value()))
        print("Details: " + self.water_details_filed.toPlainText())
        print("----------------------------")


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    wind = ExperimentForm()
    wind.show()
    sys.exit(app.exec_())
