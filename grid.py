from PyQt5 import QtWidgets as w
# from PyQt5 import QtCore
import sys
import ports


class Window(w.QWidget):

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        grid = w.QGridLayout()

        self.click_label = w.QLabel("---")
        self.dir_label = w.QLabel("---")
        self.scroll_label = w.QLabel("---")

        grid.addWidget(w.QLabel("Clicked"), 0, 0)
        grid.addWidget(self.click_label, 0, 1)
        grid.addWidget(w.QLabel("Direction"), 1, 0)
        grid.addWidget(self.dir_label, 1, 1)
        grid.addWidget(w.QLabel("IsScrolling"), 2, 0)
        grid.addWidget(self.scroll_label, 2, 1)

        button = w.QPushButton("Connect Mouse")
        button.clicked.connect(self.read_data)
        v_box = w.QVBoxLayout()
        v_box.addWidget(button)
        v_box.addLayout(grid)

        self.setLayout(v_box)
        self.setGeometry(100, 100, 300, 300)
        self.show()

    def read_data(self):
        self.port = ports.MousePort(self.click_label, self.dir_label, self.scroll_label)
        self.port.start()
        # self.port.connect(self.port, QtCore.pyqtSignal('MOUSE_VALUE'), self.updateTexts)
        # while True:
        #     data = port.read_data()
        #     print(data)

    def updateTexts(self, val):
        self.click_label.setText(str(val.index(0)))


if __name__ == '__main__':
    app = w.QApplication(sys.argv)
    window = Window()

    sys.exit(app.exec_())
