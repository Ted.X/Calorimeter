from PyQt5.QtCore import Qt as core
from PyQt5 import QtWidgets


class StartWindow(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()
        self.init_ui()
        sshFile = "start_form.less"

        with open(sshFile, "r") as fh:
            self.setStyleSheet(fh.read())

    def init_ui(self):
        self.l = QtWidgets.QLabel("კალორიმეტრი")
        self.l.setAlignment(core.AlignCenter)

        self.info = QtWidgets.QLabel("Version 6.9 \n Made with ❤️ by Tsu Students")
        self.info.setWordWrap(True)
        self.info.setStyleSheet("font: 14px;")
        self.info.setAlignment(core.AlignRight)
        info_box = QtWidgets.QVBoxLayout()
        info_box.addStretch()
        info_box.addWidget(self.info)

        self.b1 = QtWidgets.QPushButton("ახალი ექსპერიმეტის დაწყება")
        self.b2 = QtWidgets.QPushButton("ჩანაწერების ნახვა")
        self.b3 = QtWidgets.QPushButton("პარამეტრები")

        self.b1.setObjectName("startButton")
        self.b2.setObjectName("historyButton")
        self.b3.setObjectName("settingsButton")

        h_box = QtWidgets.QHBoxLayout()

        v_box = QtWidgets.QVBoxLayout()
        v_box.addStretch()
        v_box.addWidget(self.l)
        v_box.addStretch()
        v_box.addWidget(self.b1)
        v_box.addWidget(self.b2)
        v_box.addWidget(self.b3)
        v_box.addStretch()

        h_box.addStretch(5)
        h_box.addLayout(v_box, 5)
        h_box.addLayout(info_box, 5)

        # self.b1.clicked.connect(self.kk)

        self.setLayout(h_box)
        self.setWindowTitle("Calorimeter v6.9")
        self.setFixedSize(640, 640)

        # self.show()


# app = QtWidgets.QApplication(sys.argv)
# window = Window()
# sys.exit(app.exec_())
