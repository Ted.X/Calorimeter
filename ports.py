import usb.core
import usb.util
from PyQt5 import QtCore
# decimal vendor and product values


class MousePort(QtCore.QThread):

    sig = QtCore.pyqtSignal(list)

    clickData = {
        1: "Left",
        2: "Right",
        3: "Left N Right",
        4: "Middle",
        5: "Middle N Left",
        6: "Middle N Right",
        7: "Kiddin?",
        8: "Side Button Right?",
        16: "No Way",
        32: "You Have Goddam Keyboard"
    }
    dirXData = {0: "Right", 255: "Left"}
    dirYData = {0: "Down", 255: "Up"}
    scrollData = {1: "Scrolling Up", 255: "Scrolling Down"}

    def __init__(self, clickLabel, dirLabel, scrollLabel, parent=None):
        super(MousePort, self).__init__(parent)
        self.connectMouse()

        self.label = clickLabel
        self.dirLabel = dirLabel
        self.scrollLabel = scrollLabel

        self.sig.connect(self.update)

    def update(self, lst):
        clickValue = "Click Something"
        dirValue = "Move Mouse"
        scrollValue = "Try Scroll"
        if len(lst) > 0:
            print(lst)
            clickValue = self.clickData.get(lst[0], clickValue)
            dirXValue = self.dirXData.get(lst[3])
            dirYValue = self.dirYData.get(lst[5])
            scrollValue = self.scrollData.get(lst[6], scrollValue)

            if lst[2] > 0 and lst[2] < 255:
                dirValue = dirXValue
            if lst[4] > 0 and lst[4] < 255:
                if(len(dirValue) > 0):
                    dirValue = " "
                dirValue += dirYValue

        self.label.setText(clickValue)
        self.dirLabel.setText(dirValue)
        self.scrollLabel.setText(scrollValue)

    def run(self):
        collected = 0
        attempts = 2000
        while collected < attempts:
            self.sig.emit(self.read_data())
            collected += 1
        self.reattach()

    def connectMouse(self):
        self.dev = usb.core.find()
        self.interface = 0
        self.endpoint = self.dev[0][(0, 0)][0]
        # if the OS kernel already claimed the device, which is most likely true
        # thanks to http://stackoverflow.com/questions/8218683/pyusb-cannot-set-configuration
        self.connected = False
        if self.dev.is_kernel_driver_active(self.interface):
            self.dev.detach_kernel_driver(self.interface)
            # claim the device
            usb.util.claim_interface(self.dev, self.interface)

    def read_data(self):
        data = []
        try:
            data = list(self.dev.read(self.endpoint.bEndpointAddress, self.endpoint.wMaxPacketSize))
        except usb.core.USBError as e:
            pass
            # print(e)
            # import sys
            # sys.exit()
        return data

    def reattach(self):
        # release the device
        usb.util.release_interface(self.dev, self.interface)
        # reattach the device to the OS kernel
        self.dev.attach_kernel_driver(self.interface)


if __name__ == '__main__':
    port = MousePort()
    # while True:
    #     print(port.read_data())
