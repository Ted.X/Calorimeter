from ExperimentFormActivity import ExperimentForm
from PyQt5 import QtWidgets
from StartActivity import StartWindow


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.central_widget = QtWidgets.QStackedWidget()
        self.setCentralWidget(self.central_widget)

        login_activity = StartWindow()
        experiment_form_activity = ExperimentForm()

        self.central_widget.addWidget(login_activity)
        self.central_widget.addWidget(experiment_form_activity)

        login_activity.b1.clicked.connect(lambda: self.display_activity(1))
        experiment_form_activity.back_button.clicked.connect(lambda: self.display_activity(0))

    def display_activity(self, i):
        self.central_widget.setCurrentIndex(i)


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
